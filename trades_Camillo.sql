
/*

-- The below query is for Trading History request n.1
SELECT 
      first_name
      ,last_name
	  ,op.instant
      ,op.stock
      ,op.buy
      --,op.size
      ,op.price
	  ,op2.instant
      ,op2.buy
     -- ,op2.size
      ,op2.price
  FROM master.dbo.trader t
  LEFT JOIN position p on t.id=p.trader_id
  LEFT JOIN trade op on p.opening_trade_ID=op.id
  LEFT JOIN trade op2 on p.closing_trade_ID=op2.id
  

 -- The below query is for Trading History request n.2
  
  select 
	trader.first_name 
	,trader.last_name
	,b.trading_date
	,sells_total
	,buy_total
	, case when sells_total-buy_total <0 then 'Loss' else 'Gain' end as Result 
	, sells_total - buy_total as Value
  from  trader 
  join
    (SELECT 
	   t.id
	  ,CAST(op.instant as DATE) as trading_date
      ,SUM(op.size * op.price) as sells_total
  FROM master.dbo.trader t
  LEFT JOIN position p on t.id=p.trader_id
  LEFT JOIN trade op on p.opening_trade_ID=op.id or p.closing_trade_ID=op.id 
  WHERE op.buy=0
  GROUP BY
     t.id 
	,CAST(op.instant as DATE)
     ) as b on trader.id=b.id
  join
      (SELECT 
	   t.id
	  ,CAST(op.instant as DATE) as trading_date
      ,SUM(op.size * op.price) as buy_total
  FROM master.dbo.trader t
  LEFT JOIN position p on t.id=p.trader_id
  LEFT JOIN trade op on p.opening_trade_ID=op.id or p.closing_trade_ID=op.id 
  WHERE op.buy=1
  GROUP BY
     t.id 
	,CAST(op.instant as DATE)
     ) as b2 on trader.id=b2.id


 -- The below query is for Trading History request n.3

create view profit_loss_data as
 select 
	trader.first_name 
	,trader.last_name
	,b.trading_date
	,sells_total
	,buy_total
	, case when sells_total-buy_total <0 then 'Loss' else 'Gain' end as Result 
	, sells_total - buy_total as Value
  from  trader 
  join
    (SELECT 
	   t.id
	  ,CAST(op.instant as DATE) as trading_date
      ,SUM(op.size * op.price) as sells_total
  FROM master.dbo.trader t
  LEFT JOIN position p on t.id=p.trader_id
  LEFT JOIN trade op on p.opening_trade_ID=op.id or p.closing_trade_ID=op.id 
  WHERE op.buy=0
  GROUP BY
     t.id 
	,CAST(op.instant as DATE)
     ) as b on trader.id=b.id
  join
      (SELECT 
	   t.id
	  ,CAST(op.instant as DATE) as trading_date
      ,SUM(op.size * op.price) as buy_total
  FROM master.dbo.trader t
  LEFT JOIN position p on t.id=p.trader_id
  LEFT JOIN trade op on p.opening_trade_ID=op.id or p.closing_trade_ID=op.id 
  WHERE op.buy=1
  GROUP BY
     t.id 
	,CAST(op.instant as DATE)
     ) as b2 on trader.id=b2.id;

*/